import requests
from bs4 import BeautifulSoup
import pickle
from bcolors import * 
import os.path


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
sites = ["http://google.com", "http://reddit.com", "http://apple.com"]
newdict = {}

def get_script_srcs(site):
    mysrcs = []
    try:
        r = requests.get(site, headers=headers, timeout=5.000)
        soup = BeautifulSoup(r.text, "html.parser")
        result = soup.find_all('script', src=True)
    except:
        result = []
    
    for a in result:
        mysrcs.append(a.get('src'))
    
    return mysrcs

def get_new_data(sites):
    for site in sites:
        newdict[site] = get_script_srcs(site)
    return newdict

def get_old_data():
    if os.path.exists("save.p"):
        olddict = pickle.load(open("save.p", "rb" ))
    else:
        olddict =  {}  
    return olddict

def save_current_data(newdict):
    pickle.dump( newdict, open( "save.p", "wb" ) )    
    return


olddict = get_old_data()    
newdict = get_new_data(sites)
save_current_data(newdict)

for val in newdict.keys():
    print "\n"
    if val in olddict.keys():
        print bcolors.OKGREEN + str(val) + " is an existing site." + bcolors.ENDC
        if set(newdict[val]) == set(olddict[val]):
            print "\t" + bcolors.OKGREEN + " * scripts have no changes." + bcolors.ENDC 
        else:
            for entry in newdict[val]:
                if entry not in olddict[val]:
                    print "\t" + bcolors.WARNING + str(entry) + " is a new script. " + bcolors.ENDC
    else:
        print bcolors.WARNING +  str(val) + " is a new entry" + bcolors.ENDC
